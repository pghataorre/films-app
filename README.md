## Pre Installation

Please make sure you are using node version 8 or greater.
Please open a command line and use the following to check:

`node --version` 

## Installation

Clone repository the repository below by using the command below on chosen location on your machine:

`git clone https://pghataorre@bitbucket.org/pghataorre/films-app.git`



Navigate to the root where a package.json resides
Then the app needs installing:

`yarn install`

Run the app in the development mode:

`yarn start`


Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## To Do's

1. Pagination - Would like to work on via API paging
2. Genre selection from a checkbox or drop down --- in progress on a feature - feature/genre-selection still work in progress and not complete.
3. Video preview

(FAO: Zone Interviewers - If i'm given more time I would be happy to add the above features - ive spend approximately 8 hours today on this so far.)
