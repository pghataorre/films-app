const common = {
  ratingPercentage: (ratingValue) => {
    return ratingValue >= 100 ? 100 : ratingValue;
  },
  setFixedValue: (itemValue, fixTo) => {
    const fixedTo = fixTo ? fixTo: 0;

    if (itemValue) {
      return itemValue.toFixed(fixedTo);
    } 
    return itemValue;
  },
  sortFilmGenre: (genre, filmGenres) => {
    let genreString = '';

    if (genre.length > 0  && filmGenres) {
      genre.forEach((genreItemId)=> {
        filmGenres.forEach((filmGenreIdItem) => {
          if (genreItemId.id === filmGenreIdItem) {
            genreString += `${genreItemId.name}, `;
          }
        })
      })

      const trimmedgenreString = genreString.substring(0, genreString.length - 2);
      return trimmedgenreString;
    }
  }
}

export default common;
