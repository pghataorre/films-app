import React from 'react';
import common from '../common/common.js';

const RatingBar = (props) => {
  return (
    <span className="film-detail__info--rating-bar">
      <span className="film-detail__info--bar-size" style={{width : `${common.ratingPercentage(props.ratingValue)}%`}} ></span>
    </span>
  );
}

export default RatingBar;
