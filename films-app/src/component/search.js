import React, {Component} from 'react';
import { connect } from 'react-redux';
import actions from '../actions/actions.js';

class Inputsearch extends Component {
  searchFilms = (event) => {
    event.preventDefault();
    const searchVal = event.target.elements[0].value;

    if (searchVal !== '') {
      const searchValue = searchVal.trim().toLowerCase()
      this.props.searchFilms(searchValue)
    }
  }

  render() { 
  return (
      <section className="form-search">
        <form method="get" onSubmit={ this.searchFilms }>
          <label htmlFor="search">Search for films:</label>
          <input type="text" id="search" placeholder="Search here"/>
          <button type="submit" id="submit-search"><span>Search film</span></button>
        </form>
      </section>
    )
  }
}

const mapStateToProps = () => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchFilms: (searchValue) => {
      dispatch(actions.searchFilms(searchValue))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Inputsearch);