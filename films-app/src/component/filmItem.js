import React from 'react';
import config from '../config.js';
import Ratingbar from './filmRatingBar.js';
import common from '../common/common.js';
import Moment from 'react-moment';
import 'moment-timezone';

const Filmitem = (props) => {
  const {title, overview, popularity, vote_average, release_date} = props.filmItem;
  const imageName = props.filmItem.poster_path;
  const imagePath = `${config.imageUrl}${imageName}`;
  const voteAveragePercentage = vote_average * 10;
  const genre = props.genre;

  return (
    <ul className="film-detail">
      <li className="film-detail__image-list-container">
          <img src={imagePath} alt={`Film cover ${title}`} data-test="film-image"/>
      </li>
      <li className="film-detail__film-detail-container">
        <p className="film-detail__header">Title:</p>
        <p className="film-detail__info film-detail__info--title" data-test="film-title">{title}</p>
        <p className="film-detail__header">Overview</p>
        <p className="film-detail__info" data-test="film-overview">{overview}</p>
        <p className="film-detail__header">Release Date</p>
        <p className="film-detail__info">
          <Moment format="DD/MM/YYYY" data-test="film-release-date">{release_date}</Moment>
        </p>
        <p className="film-detail__header">Genre</p>
        <p className="film-detail__info" data-test="film-genre">
          {genre}
        </p>
        <p className="film-detail__header">Ratings</p>
        <p className="film-detail__info film-detail__info--rating">
          <span className="film-detail__info" >Vote average: <br />
            <span data-test="film-vote-average">{common.setFixedValue(vote_average, 2)}</span>
            <Ratingbar ratingValue={voteAveragePercentage}/>
          </span>
          <span className="film-detail__info">Popularity: <br />
            <span data-test="film-vote-popularity">{`${common.setFixedValue(popularity, 2)}%`}</span>
            <Ratingbar ratingValue={popularity}/>
          </span>
        </p>
      </li>
    </ul>
  )
}

export default Filmitem;
