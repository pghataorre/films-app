import React, { Component } from 'react';
import { connect } from 'react-redux';
import Filmitem from './filmItem.js'
import FilmTextHeader from './filmListHeaderText.js';
import common from '../common/common.js';

class FilmList extends Component {
  render() {
    const filmsCollection = this.props.filmsCollection;
    
    return (
      <section className="film-list-container" data-test="films-list-section">
        <FilmTextHeader />
        <ol>
          { filmsCollection.map((item, index) => {
              return (
                <li key={index} className="film-list-container__film-list" data-test="films-listing">
                  <Filmitem filmItem={item} genre={ common.sortFilmGenre(this.props.genre, item.genre_ids) }/>
                </li>
              )
            })
          }
        </ol>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    filmsCollection: state.filmsCollection,
    genre: state.genre
  }
}

export default connect(mapStateToProps)(FilmList);
