import React from 'react';

const Header = () => {
  
  return (
    <header data-test="films-app-header">
      <h1>Films App</h1>
    </header>
  );
}

export default Header;