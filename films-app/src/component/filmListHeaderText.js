import React from 'react';
import { connect } from 'react-redux';

const FilmListingHeader = (props) => {

  return (<h2>{setHeaderTitle(props)}</h2>)
}

const setHeaderTitle = (props) => {
  if (props.apiError) {
      return 'Sorry there has been an error please try again later'
  } else if (props.filmsCollection.length > 0 && !props.apiError) {
      return 'Films results found below:'
  } else {
      return ''
  }
}

const mapStateToProps = (state) => {
  return {
    apiError: state.apiError,
    filmsCollection: state.filmsCollection
  }
}



export default connect(mapStateToProps)(FilmListingHeader);