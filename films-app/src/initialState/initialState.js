const initState = {
  genre: [],
  defaultContentMessage: 'Please search for films.',
  filmsCount: 0,
  showOverlay: false,
  overlayImageUrl: '',
  filmsCollection: [],
  apiError: false
};

export default initState;