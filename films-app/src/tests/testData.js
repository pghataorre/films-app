const mockAction = {
  "type": "FETCH_FILMS",
  "response": {
    "page": 1,
    "total_results": 58,
    "total_pages": 3,
    "results": [
      {
        "popularity": 12.545,
        "vote_count": 1525,
        "video": false,
        "poster_path": "/vqzNJRH4YyquRiWxCCOH0aXggHI.jpg",
        "id": 290859,
        "adult": false,
        "backdrop_path": "/riTANvQ8GKmQbgtC1ps3OfkU43A.jpg",
        "original_language": "en",
        "original_title": "Terminator: Dark Fate",
        "genre_ids": [
          28,
          878
        ],
        "title": "Terminator: Dark Fate",
        "vote_average": 6.2,
        "overview": "Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.",
        "release_date": "2019-10-23"
      },
      {
        "popularity": 50.545,
        "vote_count": 12,
        "video": false,
        "poster_path": "/vqzNJRH4YyquRiWxCCOH0aXggHI.jpg",
        "id": 2923230859,
        "adult": false,
        "backdrop_path": "/riTANvQ8GKmQbgtC1ps3OfkU43A.jpg",
        "original_language": "en",
        "original_title": "TEST FILM TITLE",
        "genre_ids": [
          28,
          878
        ],
        "title": "TEST FILM TITLE",
        "vote_average": 6.2,
        "overview": "Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.",
        "release_date": "2019-10-23"
      }
    ]
  }
}

const mockState = {
  "genre": [
    {
      "id": 28,
      "name": "Action"
    }
  ],
  "defaultContentMessage": "Please search for films.",
  "filmsCount": 0,
  "showOverlay": false,
  "overlayImageUrl": "",
  "filmsCollection": [],
  "apiError": false
}


const mockResponse = {
  "genre": [
    {
      "id": 28,
      "name": "Action"
    }
  ],
  "defaultContentMessage": "Please search for films.",
  "filmsCount": 58,
  "showOverlay": false,
  "overlayImageUrl": "",
  "filmsCollection": [
    {
      "popularity": 50.545,
      "vote_count": 12,
      "video": false,
      "poster_path": "/vqzNJRH4YyquRiWxCCOH0aXggHI.jpg",
      "id": 2923230859,
      "adult": false,
      "backdrop_path": "/riTANvQ8GKmQbgtC1ps3OfkU43A.jpg",
      "original_language": "en",
      "original_title": "TEST FILM TITLE",
      "genre_ids": [
        28,
        878
      ],
      "title": "TEST FILM TITLE",
      "vote_average": 6.2,
      "overview": "Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.",
      "release_date": "2019-10-23"
    },
    {
      "popularity": 12.545,
      "vote_count": 1525,
      "video": false,
      "poster_path": "/vqzNJRH4YyquRiWxCCOH0aXggHI.jpg",
      "id": 290859,
      "adult": false,
      "backdrop_path": "/riTANvQ8GKmQbgtC1ps3OfkU43A.jpg",
      "original_language": "en",
      "original_title": "Terminator: Dark Fate",
      "genre_ids": [
        28,
        878
      ],
      "title": "Terminator: Dark Fate",
      "vote_average": 6.2,
      "overview": "Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.",
      "release_date": "2019-10-23"
    },
  ],
  "apiError": false
}

const mockGenreState = {
  "genre": [],
  "defaultContentMessage": "Please search for films.",
  "filmsCount": 0,
  "showOverlay": false,
  "overlayImageUrl": "",
  "filmsCollection": [],
  "apiError": false
}

const mockErrorState = {
  "genre": [],
  "defaultContentMessage": "Please search for films.",
  "filmsCount": 0,
  "showOverlay": false,
  "overlayImageUrl": "",
  "filmsCollection": [],
  "apiError": false
}

const mockGenreAction = {
  "type": "FETCH_GENRES",
  "genre": [
    {
      "id": 28,
      "name": "Action"
    },
    {
      "id": 12,
      "name": "Adventure"
    },
    {
      "id": 16,
      "name": "Animation"
    },
    {
      "id": 35,
      "name": "Comedy"
    },
    {
      "id": 80,
      "name": "Crime"
    },
    {
      "id": 99,
      "name": "Documentary"
    },
    {
      "id": 18,
      "name": "Drama"
    },
    {
      "id": 10751,
      "name": "Family"
    },
    {
      "id": 14,
      "name": "Fantasy"
    },
    {
      "id": 36,
      "name": "History"
    },
    {
      "id": 27,
      "name": "Horror"
    },
    {
      "id": 10402,
      "name": "Music"
    },
    {
      "id": 9648,
      "name": "Mystery"
    },
    {
      "id": 10749,
      "name": "Romance"
    },
    {
      "id": 878,
      "name": "Science Fiction"
    },
    {
      "id": 10770,
      "name": "TV Movie"
    },
    {
      "id": 53,
      "name": "Thriller"
    },
    {
      "id": 10752,
      "name": "War"
    },
    {
      "id": 37,
      "name": "Western"
    }
  ]
}

const mockGenreResponse = {
  "genre": [
    {
      "id": 28,
      "name": "Action"
    },
    {
      "id": 12,
      "name": "Adventure"
    },
    {
      "id": 16,
      "name": "Animation"
    },
    {
      "id": 35,
      "name": "Comedy"
    },
    {
      "id": 80,
      "name": "Crime"
    },
    {
      "id": 99,
      "name": "Documentary"
    },
    {
      "id": 18,
      "name": "Drama"
    },
    {
      "id": 10751,
      "name": "Family"
    },
    {
      "id": 14,
      "name": "Fantasy"
    },
    {
      "id": 36,
      "name": "History"
    },
    {
      "id": 27,
      "name": "Horror"
    },
    {
      "id": 10402,
      "name": "Music"
    },
    {
      "id": 9648,
      "name": "Mystery"
    },
    {
      "id": 10749,
      "name": "Romance"
    },
    {
      "id": 878,
      "name": "Science Fiction"
    },
    {
      "id": 10770,
      "name": "TV Movie"
    },
    {
      "id": 53,
      "name": "Thriller"
    },
    {
      "id": 10752,
      "name": "War"
    },
    {
      "id": 37,
      "name": "Western"
    }
  ],
  "defaultContentMessage": "Please search for films.",
  "filmsCount": 0,
  "showOverlay": false,
  "overlayImageUrl": "",
  "filmsCollection": [],
  "apiError": false
}

const defaultGenre = [
  {
    "id": 28,
    "name": "Action"
  },
  {
    "id": 12,
    "name": "Adventure"
  },
  {
    "id": 16,
    "name": "Animation"
  },
  {
    "id": 35,
    "name": "Comedy"
  },
  {
    "id": 80,
    "name": "Crime"
  },
  {
    "id": 99,
    "name": "Documentary"
  },
  {
    "id": 18,
    "name": "Drama"
  },
  {
    "id": 10751,
    "name": "Family"
  },
  {
    "id": 14,
    "name": "Fantasy"
  },
  {
    "id": 36,
    "name": "History"
  },
  {
    "id": 27,
    "name": "Horror"
  },
  {
    "id": 10402,
    "name": "Music"
  },
  {
    "id": 9648,
    "name": "Mystery"
  },
  {
    "id": 10749,
    "name": "Romance"
  },
  {
    "id": 878,
    "name": "Science Fiction"
  },
  {
    "id": 10770,
    "name": "TV Movie"
  },
  {
    "id": 53,
    "name": "Thriller"
  },
  {
    "id": 10752,
    "name": "War"
  },
  {
    "id": 37,
    "name": "Western"
  }
];

const defaultFilmCollection =  [
  {
    "popularity": 83.545,
    "vote_count": 1525,
    "video": false,
    "poster_path": "/vqzNJRH4YyquRiWxCCOH0aXggHI.jpg",
    "id": 290859,
    "adult": false,
    "backdrop_path": "/riTANvQ8GKmQbgtC1ps3OfkU43A.jpg",
    "original_language": "en",
    "original_title": "Terminator: Dark Fate",
    "genre_ids": [
      28,
      878
    ],
    "title": "Terminator: Dark Fate",
    "vote_average": 6.2,
    "overview": "Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.",
    "release_date": "2019-10-23"
  },
  {
    "popularity": 39.648,
    "vote_count": 5825,
    "video": false,
    "poster_path": "/5JU9ytZJyR3zmClGmVm9q4Geqbd.jpg",
    "id": 87101,
    "adult": false,
    "backdrop_path": "/3DmxSJ08m9wwbxwbwHzA4dunGNO.jpg",
    "original_language": "en",
    "original_title": "Terminator Genisys",
    "genre_ids": [
      28,
      12,
      878,
      53
    ],
    "title": "Terminator Genisys",
    "vote_average": 5.9,
    "overview": "The year is 2029. John Connor, leader of the resistance continues the war against the machines. At the Los Angeles offensive, John's fears of the unknown future begin to emerge when TECOM spies reveal a new plot by SkyNet that will attack him from both fronts; past and future, and will ultimately change warfare forever.",
    "release_date": "2015-06-23"
  },
  {
    "popularity": 30.54,
    "vote_count": 7606,
    "video": false,
    "poster_path": "/2y4dmgWYRMYXdD1UyJVcn2HSd1D.jpg",
    "id": 280,
    "adult": false,
    "backdrop_path": "/d9AqtruwS8nljKjL5aYzM42hQJr.jpg",
    "original_language": "en",
    "original_title": "Terminator 2: Judgment Day",
    "genre_ids": [
      28,
      878,
      53
    ],
    "title": "Terminator 2: Judgment Day",
    "vote_average": 8,
    "overview": "Nearly 10 years have passed since Sarah Connor was targeted for termination by a cyborg from the future. Now her son, John, the future leader of the resistance, is the target for a newer, more deadly terminator. Once again, the resistance has managed to send a protector back to attempt to save John and his mother Sarah.",
    "release_date": "1991-07-03"
  },
  {
    "popularity": 29.024,
    "id": 218,
    "video": false,
    "vote_count": 7792,
    "vote_average": 7.5,
    "title": "The Terminator",
    "release_date": "1984-10-26",
    "original_language": "en",
    "original_title": "The Terminator",
    "genre_ids": [
      28,
      53,
      878
    ],
    "backdrop_path": "/6yFoLNQgFdVbA8TZMdfgVpszOla.jpg",
    "adult": false,
    "overview": "In the post-apocalyptic future, reigning tyrannical supercomputers teleport a cyborg assassin known as the \"Terminator\" back to 1984 to kill Sarah Connor, whose unborn son is destined to lead insurgents against 21st century mechanical hegemony. Meanwhile, the human-resistance movement dispatches a lone warrior to safeguard Sarah. Can he stop the virtually indestructible killing machine?",
    "poster_path": "/q8ffBuxQlYOHrvPniLgCbmKK4Lv.jpg"
  },
  {
    "popularity": 24.529,
    "vote_count": 4179,
    "video": false,
    "poster_path": "/gw6JhlekZgtKUFlDTezq3j5JEPK.jpg",
    "id": 534,
    "adult": false,
    "backdrop_path": "/mglXOOyXISvuIdLgvhU9oXn1Mi7.jpg",
    "original_language": "en",
    "original_title": "Terminator Salvation",
    "genre_ids": [
      28,
      878,
      53
    ],
    "title": "Terminator Salvation",
    "vote_average": 6,
    "overview": "All grown up in post-apocalyptic 2018, John Connor must lead the resistance of humans against the increasingly dominating militaristic robots. But when Marcus Wright appears, his existence confuses the mission as Connor tries to determine whether Wright has come from the future or the past -- and whether he's friend or foe.",
    "release_date": "2009-05-20"
  }
];

const testData = {
  mockAction,
  mockState,
  mockResponse,
  mockGenreAction,
  mockGenreState,
  mockGenreResponse,
  mockErrorState,
  defaultGenre,
  defaultFilmCollection
}

export default testData