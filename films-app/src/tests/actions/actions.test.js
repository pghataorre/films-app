import config from '../../config.js';
import actions from '../../actions/actions.js';

describe('Actions -  __searchFilmAction()', () => {
  it('Should return mocked Response', () => {
    const mockResponse = [{results: [{}, {}, {}]}];
    expect(actions.__searchFilmAction(mockResponse)).toEqual({"response": mockResponse, "type": "FETCH_FILMS"});
  });
});

describe('Actions -  __dispatchFilmSearchError()', () => {
  it('Should return false', () => {
    const inputParam = false;
    expect(actions.__dispatchFilmSearchError(inputParam)).toEqual({"error": false, "type": "FETCH_FILMS_ERROR"});
  });

  it('Should return true', () => {
    const inputParam = true;
    expect(actions.__dispatchFilmSearchError(inputParam)).toEqual({"error": true, "type": "FETCH_FILMS_ERROR"});
  });
});

describe('Actions -  searchFilms()', () => {
  it('Should return back correct response', () => {
    //const fetchSpy = jest.spyOn(window, 'fetch');

    //const output = actions.searchFilms(inputParam);
    //console.log(`\n======================================\n output= ${output}\n======================================\n`);
    //expect(output).toEqual(resp);

  });
});