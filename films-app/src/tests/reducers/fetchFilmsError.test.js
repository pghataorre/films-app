import fetchFilmsError from '../../reducers/fetchFilmsError.js';
import testData from '../testData.js';

describe('Test fetchFilmsError reducer', () => {
  it('Should return correct response', () => {
    const mockAction = {error: true}
    const fetchFilmsErrorResponse = fetchFilmsError(testData.mockErrorState, mockAction).apiError
    expect(fetchFilmsErrorResponse).toEqual(true);
  });
});