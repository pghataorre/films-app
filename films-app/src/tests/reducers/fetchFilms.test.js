import fetchFilms from '../../reducers/fetchFilms.js';
import testData from '../testData.js';

describe('Test fetccFilms reducer', () => {
  it('Should return correct response', () => {
    expect(fetchFilms(testData.mockState, testData.mockAction)).toEqual(testData.mockResponse);
  });

  it('Should return correct response in correct order by higest popularity', () => {
    const filmPopularity = fetchFilms(testData.mockState, testData.mockAction).filmsCollection[0].popularity;
    expect(filmPopularity).toEqual(50.545);
  });
});