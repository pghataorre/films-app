import fetchGenres from '../../reducers/fetchGenre.js';
import testData from '../testData.js';

describe('Test fetchGenres reducer', () => {
  it('Should return correct response', () => {
    expect(fetchGenres(testData.mockGenreState, testData.mockGenreAction)).toEqual(testData.mockGenreResponse);
  });
});