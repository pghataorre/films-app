import commonFunctions from '../../common/common.js';
import testData from '../testData.js';

describe('Common functions', () => {
  it('Test ratingPercentage() - Should return a same value < 100', () => {
    const mockPercentage = 22.20;
    const testPercentageOutput = commonFunctions.ratingPercentage(mockPercentage);
    expect(testPercentageOutput).toEqual(mockPercentage);
  });

  it('Test ratingPercentage() - Should return 100 when value is > 100 ', () => {
    const mockPercentage = 101.1;
    const testPercentageOutput = commonFunctions.ratingPercentage(mockPercentage);
    expect(testPercentageOutput).toEqual(100);
  })

  it('Test setFixedValue() should round a decimal to X decimal places', () => {
    const fixTo = 2;
    let mockValue = 10.153234234;
    let testFixedOutput = commonFunctions.setFixedValue(mockValue, fixTo);
    expect(testFixedOutput).toEqual('10.15');
    
    mockValue = 10.155234234;
    testFixedOutput = commonFunctions.setFixedValue(mockValue, 2);
    expect(testFixedOutput).toEqual('10.16');
  });

  it('Test setFixedValue() should round a decimal to an integer if a fixed point ', () => {
    let mockValue = 10.153234234;
    let testFixedOutput = commonFunctions.setFixedValue(mockValue);
    expect(testFixedOutput).toEqual('10');
  });

  it('Test sortFilmGenre() should return the correct film genre', () => {
    const testGenreData = testData.defaultGenre;
    const mockGenreIds = [28, 53, 878];
    let genreOutput = commonFunctions.sortFilmGenre(testGenreData, mockGenreIds);
    expect(genreOutput).toEqual('Action, Science Fiction, Thriller');

    const singleMockGenreIds = [28];
    genreOutput = commonFunctions.sortFilmGenre(testGenreData, singleMockGenreIds);
    expect(genreOutput).toEqual('Action');

    const noMockGenreIds = [];
    genreOutput = commonFunctions.sortFilmGenre(testGenreData, noMockGenreIds);
    expect(genreOutput).toEqual('');
  });

});