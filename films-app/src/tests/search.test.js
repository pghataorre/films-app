import React from 'react';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store';
import InputSearch from '../component/search.js';
import store from '../initialState/initialState';

describe('Search component', () => {
  const configStore = configureStore([thunk]);
  const mockStore = configStore(store);

  it('Shoud render search component', () => {
    const mockSearchFilms = jest.fn();
    const component = mount(<Provider store={mockStore}><InputSearch onSubmit={mockSearchFilms} /></Provider>)

    expect(component).toMatchSnapshot();
    expect(component.find('label').text().trim()).toEqual('Search for films:');
    expect(component.find('#search').length).toEqual(1);
    expect(component.find('#submit-search').length).toEqual(1);
    // TO DO
    // component.find('#submit-search').simulate('click', { preventDefault: () => {} });
    // expect(mockSearchFilms).toHaveBeenCalled();
    expect(component.find('#search').instance().value = "Test film").toEqual('Test film');
  });
});

// console.log(`\n======================================\n mockSearchFilms= ${mockSearchFilms}\n======================================\n`);