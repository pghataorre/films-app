import React from 'react';
import { shallow } from 'enzyme';
import Header from '../component/header';

describe('Header Componenet', () => {
  it('Should render header', () => {
    const component = shallow(<Header />);
    const wrapper = component.find(`[data-test='films-app-header']`);
    expect(wrapper.length).toBe(1);
  });

  it('Should render correct header title', () => {
    const component = shallow(<Header />);
    const headerText = component.find('h1')
    expect(component).toMatchSnapshot();
    expect(headerText.text()).toEqual('Films App')
  });
});

