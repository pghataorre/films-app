import React from 'react';
import { mount, shallow } from 'enzyme';
import FilmItem from '../component/filmItem.js';
import testData from './testData.js';

describe('Film Item', () => {
  it('Should render with correct film information', () => {
    const filmsItem = testData.defaultFilmCollection[0];
    const mockGenre = 'Test genre';
    const component = shallow(<FilmItem filmItem={filmsItem} genre={mockGenre} />)

    expect(component.find(`[data-test='film-title']`).text().trim()).toEqual('Terminator: Dark Fate');
    expect(component.find(`[data-test='film-overview']`).text().trim()).toEqual('Decades after Sarah Connor prevented Judgment Day, a lethal new Terminator is sent to eliminate the future leader of the resistance. In a fight to save mankind, battle-hardened Sarah Connor teams up with an unexpected ally and an enhanced super soldier to stop the deadliest Terminator yet.');
    // expect(component.find(`time[data-test='film-release-date]`).text().trim()).toEqual('23/10/2019');
    expect(component.find(`[data-test='film-genre']`).text().trim()).toEqual('Test genre');
    expect(component.find(`[data-test='film-vote-average']`).text().trim()).toEqual('6.20');
    expect(component.find(`[data-test='film-vote-popularity']`).text().trim()).toEqual('83.55%');
  });
});
