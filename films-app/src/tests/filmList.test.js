import React from 'react';
import { Provider } from 'react-redux';
import { mount, shallow } from 'enzyme';
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store';
import FilmList from '../component/filmList.js';
import store from '../initialState/initialState';
import testData from './testData.js';

describe('Film list component', () => {
  it('Shoud render a list of films', () => {
    const mockListStore = Object.assign({}, store, {
      filmsCollection: testData.defaultFilmCollection,
      genre: testData.defaultGenre,
      apiError: false
    });

    const configStore = configureStore([thunk]);
    const mockStore = configStore(mockListStore);
    const component = mount(<Provider store={mockStore}><FilmList /></Provider>)

    expect(component).toMatchSnapshot();
    expect(component.find(`[data-test='films-list-section']`).length).toEqual(1);
    expect(component.find(`[data-test='films-listing']`).length).toEqual(5);
  });
});

// console.log(`\n======================================\n mockSearchFilms= ${mockSearchFilms}\n======================================\n`);