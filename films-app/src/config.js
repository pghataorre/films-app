const config = {
  apiUrl: 'https://api.themoviedb.org/3/search/movie',
  apiGenre: 'https://api.themoviedb.org/3/genre/movie/list',
  apiKey: '2aa9587fc202f0d2d58cb41b51165c6d',
  adultContent: false,
  defaultSearch: 'Space',
  defaultImageType: 'Photo',
  notificationTextNoContent: 'Sorry no content avaialble try again.',
  imageUrl: 'https://image.tmdb.org/t/p/w780/'
}

export default config