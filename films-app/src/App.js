import React, {Component} from 'react';
import './styles/main.scss';
import Header from './component/header.js';
import Search from './component/search.js';
import Filmlist from './component/filmList.js';
import { connect } from 'react-redux';
import config from './config';

class App extends Component {
  componentDidMount() {
    const url = `${config.apiGenre}?api_key=${config.apiKey}`

    fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        return this.props.dispatchGenres(response.genres);
      })
      .catch((err) => {
        this.props.dispatchFilmSearchError(true);
        console.log('ERROR')
      });
  }

  render() {
    return (
      <div>
        <Header />
        <main>
          <Search />
          <Filmlist />
        </main>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchGenres: (genre) => {
      dispatch({
        type: 'FETCH_GENRES',
        genre
      });
    },
    dispatchFilmSearchError: (error) => {
      dispatch({
        type: 'FETCH_FILMS_ERROR',
        error
      })
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
