
import config from '../config.js';

const searchFilms = (searchValue) => {
  const url = `${config.apiUrl}?api_key=${config.apiKey}&language=en-US&query=${searchValue}&page=1&include_adult=${config.adultContent}`; 
 
  return (dispatch) => {
    return fetch(url)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (!response.success && response.status_message) {
          dispatch(__dispatchFilmSearchError(true))
        }

        dispatch(__searchFilmAction(response));
      })
      .catch((err) => {
        return dispatch(__dispatchFilmSearchError(true))
      });
    }
}

const __searchFilmAction = (response) => {
  return {
    type: 'FETCH_FILMS',
    response
  }
}

const __dispatchFilmSearchError = (error) => {
  return {
    type: 'FETCH_FILMS_ERROR',
    error
  }
}

export const actions = {
  searchFilms,
  __searchFilmAction,
  __dispatchFilmSearchError,
};

export default actions;