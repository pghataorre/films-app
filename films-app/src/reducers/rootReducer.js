import initState from '../initialState/initialState.js';
import fetchFilms from './fetchFilms.js';
import fetchFilmsError from './fetchFilmsError.js';
import fetchGenres from './fetchGenre';

const rootReducer  = (state = initState, action) => {
  switch(action.type) {
    case 'FETCH_FILMS':
      return fetchFilms(state, action);

    case 'FETCH_FILMS_ERROR': 
      return fetchFilmsError(state, action);

      case 'FETCH_GENRES':
        return fetchGenres(state, action);

    default: 
      return state;
  }
}

export default rootReducer;
