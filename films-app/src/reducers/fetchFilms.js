const fetchFilms = (state, action) => {
  const filmsCount = action.response.total_results;
  const filmsCollection = action.response.results;
  filmsCollection.sort((a, b) => {
    return b.popularity - a.popularity;
  });
  
  return Object.assign({}, state, {
    filmsCollection,
    filmsCount,
    apiError: false
  })
}

export default fetchFilms;