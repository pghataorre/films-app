const fetchFilmsError = (state, action) => {
  const filmsCollection = [];
  const apiError = action.error;

  return Object.assign({}, state, {
    filmsCollection,
    apiError
  });
}

export default fetchFilmsError;