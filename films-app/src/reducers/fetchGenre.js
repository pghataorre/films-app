const fetchGenres = (state, action) => {
  const genre = action.genre;

  return Object.assign({}, state, {
    genre
  })
}

export default fetchGenres;
